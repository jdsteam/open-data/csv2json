var express = require('express');
var app = express();
var fs = require("fs");
var http = require('http');
var url = require('url');
var urlExists = require('url-exists');

app.get('/csv2json', function (req, res) {
    
    if(req.query.url){
		
		var callback = req.query.callback;
		var urls = req.query.url;
		var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
		var request = new XMLHttpRequest();
	
        request.open('GET', urls, true);
        request.send(null);
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status === 200) {
                result = CSVToArray(request.responseText);
                result["url"] = urls;
                res.end(callback+"("+JSON.stringify(result)+")");
                // res.json(result); 
            }
        }
		
    }else{
        var ress = {};
        ress["fields"] = null;
        ress["data"] = null;
		ress["msg"] = "url not found.";
        res.end(JSON.stringify(ress))
    }
})

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   console.log("Example app listening at http://127.0.0.1:8081")
})

function CSVToArray( strData, strDelimiter ){
    strDelimiter = (strDelimiter || ",");
    var objPattern = new RegExp(
        (
            // Delimiters.
            "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +

            // Quoted fields.
            "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +

            // Standard fields.
            "([^\"\\" + strDelimiter + "\\r\\n]*))"
        ),
        "gi"
        );
    var ress = {};
    var data = [];
    
    var arrData = [];
    var headers = [];
    var headersFound = false;
    var headerIndex = 0;
    var arrMatches = null;
    var temp = [];
    while (arrMatches = objPattern.exec( strData )){
        
        var strMatchedDelimiter = arrMatches[ 1 ];
        if (strMatchedDelimiter.length && strMatchedDelimiter !== strDelimiter){
            arrData.push( {} );
            headersFound = true;
            headerIndex = 0;
        }

        var strMatchedValue;
        if (arrMatches[ 2 ]){
            strMatchedValue = arrMatches[ 2 ].replace(new RegExp( "\"\"", "g" ),"\"");
        } else {
            strMatchedValue = arrMatches[ 3 ];
        }

        if (!headersFound) {
            headers.push(strMatchedValue);
        } else {
            if(headerIndex < headers.length-1){
                temp.push(strMatchedValue);
            }else{
                temp.push(strMatchedValue);
                data.push(temp);
                temp = [];
            } 
          
          arrData[arrData.length -1][headers[headerIndex]] = headerIndex;
          headerIndex ++;
          
        }
        
        
    }
    ress["fields"] = headers;
    ress["data"] = data;
    ress["msg"] = "convert success.";
    
    return( ress );
}
